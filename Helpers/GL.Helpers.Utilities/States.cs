﻿using System;
using System.Collections.Generic;

namespace GL.Helpers.Utilities
{
    public static class States
    {
        public static List<string> GetAll()
        {
            List<string> ret = new List<string>();
            ret.Add("ACT");
            ret.Add("NSW");
            ret.Add("VIC");
            ret.Add("QLD");
            ret.Add("TAS");
            ret.Add("SA");
            ret.Add("NT");
            ret.Add("WA");

            return ret;
        }
    }
}
