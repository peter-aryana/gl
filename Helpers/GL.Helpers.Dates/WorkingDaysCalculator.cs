using GL.DataContracts.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

using GL.Framework.StdExtensions;
using System.Threading.Tasks;

namespace GL.Helpers.Dates
{
  public class WorkingDaysCalculator
  {
    public static async Task<int> GetWorkingDaysAsync(DateTime from, DateTime to, List<string> apiModelDatesAsString)
    {
          var dayDifference = (int)to.Subtract(from).TotalDays;

            if (dayDifference != 0)
            {
                return await Task.Run(() => Enumerable
                                    .Range(1, dayDifference)
                                    .Select(x => from.AddDays(x - 1))
                                    .Count(x => x.DayOfWeek != DayOfWeek.Saturday &&
                                                  x.DayOfWeek != DayOfWeek.Sunday &&
                                                  apiModelDatesAsString.Contains(x.DateToStringYYYYMMDD(), StringComparer.CurrentCultureIgnoreCase) == false));
            }


            return await Task.Run(() => Enumerable
                                    .Range(1, dayDifference)
                                    .Select(x => from.AddDays(x))
                                    .Count(x => x.DayOfWeek != DayOfWeek.Saturday &&
                                                x.DayOfWeek != DayOfWeek.Sunday &&
                                                apiModelDatesAsString.Contains(x.DateToStringYYYYMMDD(), StringComparer.CurrentCultureIgnoreCase) == false));


    }
  }
}
