﻿using System;

namespace GL.Framework.StdExtensions
{
    public static class DateExtensions
    {
        public static string DateToStringYYYYMMDD(this DateTime value)
        {
            return value.Date.ToString("yyyyMMdd");
        }
    }
}
