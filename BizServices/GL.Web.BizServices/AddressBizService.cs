using System;
using System.Collections.Generic;
using GL.DataContracts.ViewModels;

namespace GL.Web.BizServices
{
  public class AddressBizService
  {
    public AddressBizService()
    {
    }

    public List<StateViewModel> GetAustralianStates()
    {
      return new()
      {
        new() {Code = "NSW", Name = "New South Wales", CapitalCity="Sydney"},
        new() {Code = "QLD", Name = "Queensland", CapitalCity="Brisbane"},
        new() {Code = "VIC", Name = "Victoria", CapitalCity="Melbourne"},
        new() {Code = "SA", Name = "South Australia" , CapitalCity="Adelaide"},
        new() {Code = "WA", Name = "Western Australia", CapitalCity="Perth"},
        new() {Code = "TAS", Name = "Tasmania", CapitalCity="Hobart"},
        new() {Code = "ACT", Name = "Australian Capital Territory", CapitalCity = "Canberra" },
        new() {Code = "NT", Name = "Northern Territory", CapitalCity = "Darwin" }
      };
    }
  }
}
