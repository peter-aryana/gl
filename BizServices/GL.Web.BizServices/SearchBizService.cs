using System;
using System.Threading.Tasks;

using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using GL.DataContracts.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using GL.DataContracts.Exceptions;
using GL.Helpers.Dates;

namespace GL.Web.BizServices
{
  public class SearchBizService
  {
    public const string KeyGovtApp = "KeyGovtApp";
    private readonly IMemoryCache _memoryCacheService;

    public SearchBizService(IMemoryCache memoryCacheService)
    {
      _memoryCacheService = memoryCacheService;
    }

    public async Task<int> SearchAsync(SearchViewModel svModel)
    {
      if (svModel == null)
      {
        throw new SearchViewModelNullException("Search criteria is null");
      }
      svModel.DateTo = svModel.DateToParsed.AddDays(1).ToString("yyyyMMdd");
      DateTime newFromDate = (svModel.IncludeDateFrom ? svModel.DateFromParsed : svModel.DateFromParsed.AddDays(1));
      DateTime newToDate = (svModel.IncludeDateTo ? svModel.DateToParsed : svModel.DateToParsed.AddDays(-1));

      // Get the standard working days that are not Sat or Sun or a public holiday for that state.
      var gData = RetrieveGovtAppData().Where(x => x.State.Contains(svModel.State, StringComparison.CurrentCultureIgnoreCase)).Select(x => x.Date).ToList();
      return await WorkingDaysCalculator.GetWorkingDaysAsync(newFromDate, newToDate, gData);
    }

    public HashSet<ApiCallViewModel> RetrieveGovtAppData()
    {
      if (_memoryCacheService.TryGetValue(KeyGovtApp, out var cacheDataSet))
      {
        return cacheDataSet as HashSet<ApiCallViewModel>;
      }

      var dataSetDatesStates = RetrieveGovtApiData();
      _memoryCacheService.Set(KeyGovtApp, dataSetDatesStates);
      return dataSetDatesStates;
    }

    public static HashSet<ApiCallViewModel> RetrieveGovtApiData()
    {
      using (var r = new StreamReader($"{AppContext.BaseDirectory}//DataSources//australian_public_holidays.json"))
      {
        var json = r.ReadToEnd();
        var rawDataDateStates = JsonConvert.DeserializeObject<List<ApiCallViewModel>>(json);
        return rawDataDateStates?.Where(x => !string.IsNullOrWhiteSpace(x.Date) && !string.IsNullOrWhiteSpace(x.State)).ToHashSet();
      }
    }
  }
}
