using GL.DataContracts.ViewModels;
using GL.Web.BizServices;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GL.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SearchController : ControllerBase
    {
        private readonly SearchBizService _searchService;

        public SearchController(SearchBizService service)
        {
            _searchService = service;
        }

        [HttpPost("CountByObjectAsync")]
        public async Task<int> CountByObjectAsync(SearchViewModel svModel)
        {
            return await _searchService.SearchAsync(svModel);
        }
    }
}
