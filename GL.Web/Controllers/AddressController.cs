using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GL.DataContracts.ViewModels;
using GL.Web.BizServices;

namespace GL.Web.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class AddressController : ControllerBase
  {
    private readonly AddressBizService _addressBizService;

    public AddressController(AddressBizService addressBizService)
    {
      _addressBizService = addressBizService;
    }

    [HttpGet("States")]
    public IEnumerable<StateViewModel> GetStates()
    {
      return _addressBizService.GetAustralianStates();
    }

  }
}
