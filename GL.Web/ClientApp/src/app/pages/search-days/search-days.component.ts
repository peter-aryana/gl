import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';

import { State, SearchDateRequest } from '../../_models';
import { AddressService, SearchService } from '../../_services';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../../_components/confirm-dialog/confirm-dialog.component';

export const DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
}

@Component({
  selector: 'app-search-days',
  templateUrl: './search-days.component.html',
  styleUrls: ['./search-days.component.scss'],
  providers: [
    AddressService,
    SearchService,
  
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS }
  ]
})
export class SearchDaysComponent implements OnInit {

  formGroupContainer: FormGroup;
  states: State[] = [];
  displayResult: boolean = false;
  resultsInDays: number;

  constructor(private addressService: AddressService, private searchService: SearchService, public dialog: MatDialog,
    private datePipe: DatePipe)
  {
    this.formGroupContainer = new FormGroup({
      dateFrom: new FormControl(new Date(new Date().getFullYear(), 0, 1), [Validators.required]),
      dateTo: new FormControl(new Date(new Date().getFullYear() -1, 12, 31), [Validators.required]),
      state: new FormControl(null, [Validators.required]),
      includeDateFrom: new FormControl(false, [Validators.required]),
      includeDateTo: new FormControl(false, [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.initDataAsync();
  }

  async initDataAsync(): Promise<void> {
    this.states = await this.addressService.getStates();
  }

  async onClickSearchAsync($events: any): Promise<void> {

    if (this.formGroupContainer.invalid || this.formGroupContainer.pending) {
      return;
    }

    const dateFrom: Date = this.formGroupContainer.controls.dateFrom.value;
    const dateTo: Date = this.formGroupContainer.controls.dateTo.value;

    if (dateFrom >= dateTo) {
      const message = `Date From is greater than or equal to the To Date.`;
      const dialogData = new ConfirmDialogModel("Error", message);
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        maxWidth: "400px",
        data: dialogData
      });

      return;
    }

    const stateSelected = <State>this.formGroupContainer.controls.state.value;

    const data = <SearchDateRequest>{
      dateFrom: this.datePipe.transform(dateFrom, "yyyyMMdd"),
      includeDateFrom: this.formGroupContainer.controls.includeDateFrom.value,
      dateTo: this.datePipe.transform(dateTo, "yyyyMMdd"),
      includeDateTo: this.formGroupContainer.controls.includeDateTo.value,
      state: stateSelected.code
    };

    this.resultsInDays = await this.searchService.searchForWorkingDays(data);
    this.displayResult = true;
  }
}
