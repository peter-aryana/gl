import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppMaterialModule } from '../app.material.module';

import { LoaderComponent } from './app-loader/loader.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';

@NgModule({
    imports: [
      CommonModule,
      RouterModule,
      AppMaterialModule
    ],
    declarations: [
      LoaderComponent,
      NavMenuComponent,
      ConfirmDialogComponent
    ],
    exports: [
      LoaderComponent,
      NavMenuComponent,
      ConfirmDialogComponent
    ]
})
export class AppComponentsModule { }
