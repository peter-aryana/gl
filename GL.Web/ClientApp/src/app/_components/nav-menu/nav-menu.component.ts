import { Component, OnInit, Inject, HostListener, OnDestroy } from '@angular/core';
import {
    ActivationStart,
    ActivatedRouteSnapshot,
    // import as RouterEvent to avoid confusion with the DOM Event
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
} from '@angular/router'
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements OnInit {
  isExpanded = false;

  constructor() { }

  ngOnInit(): void {
  }


  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

}
