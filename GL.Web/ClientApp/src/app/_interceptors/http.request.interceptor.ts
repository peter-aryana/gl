import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";

import { LoaderService } from "../_services";
import { HeaderOptionNames } from "../_models";

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
    private finalizeTimeOut: number = 800;

    constructor(public loaderService: LoaderService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (!request.headers.keys().find(x => x === HeaderOptionNames.HIDE_LOADER)) {
            this.loaderService.show();
        }

       //@TODO check if Token has expired clone header to add bearer token..
       //request = request.clone({
       //             setHeaders: {
       //                 Authorization: `Bearer ${token}`
       //             },
       //   });
            

        return next.handle(request).pipe(
            finalize(() => {
                setTimeout(() => {
                    this.loaderService.hide();
                }, this.finalizeTimeOut);
            })
        );
    }
}
