import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app.material.module';

import { AppComponent } from './app.component';
import { LoaderService, RouteService, HttpClientService } from "./_services";
import { AppComponentsModule } from "./_components/app.components.module";
import { InjectPropertyNames } from "./_models";
import { HttpRequestInterceptor } from './_interceptors';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    AppComponentsModule,

  ],
  providers: [
    LoaderService,
    RouteService,
    HttpClientService,
    DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'en-AU' },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: InjectPropertyNames.BASE_URL, useFactory: () => { return document.getElementsByTagName('base')[0].href } },
    { provide: InjectPropertyNames.API_URL, useFactory: () => { return document.getElementsByTagName('base')[0].href + 'api'; } }

  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
