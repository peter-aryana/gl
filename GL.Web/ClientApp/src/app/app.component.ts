import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import {
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  ActivatedRouteSnapshot,
  ActivationStart
} from '@angular/router'
import { filter } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { RouteService } from "./_services/route.service";

const Title_Page = "GuildLink";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'GuildLink';

  public navigationSubscription: any;
  private navigationTitleSubscription: any;

  constructor(private titleService: Title, private routeService: RouteService) { }

  ngOnInit(): void {
    this.navigationTitleSubscription = this.routeService.router.events.pipe(filter(event => event instanceof ActivationStart)).subscribe(event => {
      this.navigationActivationStartInterceptor(<ActivationStart>event);
    });
  }

  public navigationActivationStartInterceptor(event: ActivationStart): void {
    const routeSnapshot = event['snapshot'] as ActivatedRouteSnapshot;
    const dataOptions = routeSnapshot.data;
    let pageTitle = Title_Page;

    if (dataOptions && dataOptions.title) {
      pageTitle = `${pageTitle} | ${dataOptions.title}`;
      this.titleService.setTitle(pageTitle);
    }
    this.titleService.setTitle(pageTitle);
  }

  ngOnDestroy(): void {
    // avoid memory leaks here by cleaning up after ourselves.   
    // method on every navigationEnd event.
    if (this.navigationTitleSubscription) {
      this.navigationTitleSubscription.unsubscribe();
    }
  }
}
