﻿export interface Token {
    id: string,
    auth_token: string;
    expires_in: number;
}

export interface TokenSession {
    id: string,
    userId: string;
    auth_token: string;
    expires_in: number;
    username: string;
    firstname: string;
    lastname: string;
    expirationDate: Date;
    role: string;
    roleIdentity: string;
    isRegistered: boolean;
    status: UserApplicantStatus;
}

export interface TokenStatus {
    isExpired: boolean;
    millisecondsToExpired: number;
    minutesToExpired: number;
}

export enum TokeRoleName {
    USER = "USER",
    ADMIN = "ADMIN",
}

export enum UserApplicantStatus {
    AwaitingApproval = "AwaitingApproval",
    Approved = "Approved",
    UnAuthorizedStatus = "UnAuthorizedStatus"
}

export interface RouteAllowStatus {
    isAllowed: boolean;
    notAllowedRouteTo:string;
}
