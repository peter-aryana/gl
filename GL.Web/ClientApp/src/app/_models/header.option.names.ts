export class HeaderOptionNames {
    public static HIDE_LOADER: string = 'x-content-loader-hide';
    public static CORRELATION_ID: string = 'x-correlation-id';
}
