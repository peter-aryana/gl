export interface State {
  code: string;
  name: string;
  capitalCity: string;
}
