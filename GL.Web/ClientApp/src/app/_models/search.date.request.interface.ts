export interface SearchDateRequest {
  dateFrom: string;
  includeDateFrom: boolean;
  dateTo: string;
  includeDateTo: boolean;
  state: string;
}
