import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule, MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from "@angular/material/select";
import { MatTooltipModule } from "@angular/material/tooltip";

const materialModules = [
    NgbModule,
    MatFormFieldModule,
    MatButtonModule, MatInputModule, MatProgressBarModule,
    MatRadioModule, MatCheckboxModule, MatSnackBarModule ,
    MatRippleModule, MatDialogModule, MatIconModule, MatProgressSpinnerModule, MatCardModule,
    MatMenuModule, MatSelectModule, MatTooltipModule,
    MatDatepickerModule, MatNativeDateModule, MatBadgeModule
];

@NgModule({
    declarations:[],
    imports: [materialModules],
    exports: [materialModules]
})

export class AppMaterialModule {
}
