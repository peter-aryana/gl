import { Injectable } from '@angular/core';
import { HttpClientService } from './http.client.service';
import { State, SearchDateRequest } from "../_models";

@Injectable()
export class SearchService {

  constructor(private client: HttpClientService) { }

  public searchForWorkingDays(data: SearchDateRequest): Promise<number> {
    return this.client.post<number>('Search/CountByObjectAsync', data).toPromise();
  }

}
