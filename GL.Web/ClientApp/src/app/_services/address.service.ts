import { Injectable } from '@angular/core';
import { HttpClientService } from './http.client.service';
import { State } from "../_models";

@Injectable()
export class AddressService {

  constructor(private client: HttpClientService) { }

  public getStates(): Promise<State[]> {
    return this.client.get<State[]>('address/states', this.client.headerOptionHideLoader()).toPromise();
  }

}
