import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from "@angular/common/http";
import { Observable } from 'rxjs';

import { HeaderOption, HeaderOptionNames, InjectPropertyNames } from "../_models";

@Injectable()
export class HttpClientService {

  constructor(@Inject(InjectPropertyNames.BASE_URL) public baseUrl: string,
    @Inject(InjectPropertyNames.API_URL) public apiBaseUrl: string,
    public httpClient: HttpClient) { }

  public get<T>(actionUrl: string, headerOption?: HeaderOption): Observable<T> {
    return this.httpClient.get<T>(`${this.apiBaseUrl}/${actionUrl}`, { headers: this.httpOptions(headerOption) });
  }

  public post<T>(actionUrl: string, data: any, headerOption?: HeaderOption): Observable<T> {
    return this.httpClient.post<T>(`${this.apiBaseUrl}/${actionUrl}`, data, { headers: this.httpOptions(headerOption) });
  }

  public httpOptions(headerOption?: HeaderOption) {
    let httpHeaders: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    });

    if (headerOption) {
      httpHeaders = httpHeaders.append(headerOption.key, headerOption.values);
    }
    return httpHeaders;
  }

  public headerOptionHideLoader(): HeaderOption {
    return <HeaderOption>{ key: HeaderOptionNames.HIDE_LOADER, values: ["true"] };
  }
}
