import { Injectable} from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class RouteService {

  constructor(public router: Router) { }

  public currentRouteUrlReload(): void {
    setTimeout(() => {
      this.routeToPage(this.router.url);
    }, 200);
  }

  public routeToPage(pageRouteName: string): void {
    this.router.navigate([pageRouteName]);
    this.scrollWindowToTop();
  }

  public scrollWindowToTop(): void {
    let scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }
}
