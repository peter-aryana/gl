
import { NgModule } from '@angular/core';
import { RouterModule, Routes, ExtraOptions } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { HomeComponent } from './pages/home/home.component';
import { SearchDaysComponent } from './pages/search-days/search-days.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { AppMaterialModule } from './app.material.module';


const config: ExtraOptions = {
  useHash: false,
  enableTracing: false,
  onSameUrlNavigation: 'reload'
};

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  {
    path: "search-days",
    component: SearchDaysComponent,
    data: { title: 'Days in Week' },
    runGuardsAndResolvers: "always"
  },
  {
    path: 'page-not-found', component: PageNotFoundComponent,
    data: { title: '404 PAGE NOT FOUND' }
  },
  {
    path: '404', component: PageNotFoundComponent,
    data: { title: '404 PAGE NOT FOUND' },
  },
  {
    path: '**',
    redirectTo: '404',
    data: { title: '404 PAGE NOT FOUND' },
    runGuardsAndResolvers: "always"
  }
];

@NgModule({
  declarations: [
    HomeComponent,
    SearchDaysComponent,
    PageNotFoundComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppMaterialModule,
    RouterModule.forRoot(routes, config)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
