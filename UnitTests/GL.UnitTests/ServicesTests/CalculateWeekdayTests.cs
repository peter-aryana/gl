using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GL.DataContracts.ViewModels;
using GL.Web.BizServices;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using Xunit;

namespace GL.UnitTests.ServicesTests
{
  public class CalculateWeekdayTests
  {
    private Mock<IMemoryCache> _mockCache;

    public CalculateWeekdayTests()
    {
      var mockCacheEntry = new Mock<ICacheEntry>();
      _mockCache = new Mock<IMemoryCache>();

      _mockCache
        .Setup(mc => mc.CreateEntry(It.IsAny<object>()))
        .Callback((object k) => { })
        .Returns(mockCacheEntry.Object); // this should address the null reference exception

      mockCacheEntry
        .SetupSet(mce => mce.Value = It.IsAny<object>())
        .Callback<object>(v => { });

      mockCacheEntry
        .SetupSet(mce => mce.AbsoluteExpirationRelativeToNow = It.IsAny<TimeSpan?>())
        .Callback<TimeSpan?>(dto => { });
    }

    [Theory]
    [MemberData(nameof(Data))]
    public async Task CalculateWorkingDays_ShouldReturnDaysAsync(string dateFrom, bool includeDateFrom, string dateTo, bool includeDateTo, string state, int daysReturn)
    {
      var service = new SearchBizService(_mockCache.Object);
      SearchViewModel param = new()
      {
        DateFrom = DateTime.ParseExact(dateFrom, "dd/MM/yyyy", null).ToString("yyyyMMdd"),
        IncludeDateFrom = includeDateFrom,
        DateTo = DateTime.ParseExact(dateTo, "dd/MM/yyyy", null).ToString("yyyyMMdd"),
        IncludeDateTo = includeDateTo,
        State = state
      };

      var result = await service.SearchAsync(param);
      Assert.Equal(result, daysReturn);

    }

    //Theory Test Data
    public static IEnumerable<object[]> Data =>
      new List<object[]>
      {
        new object[] { "28/04/2021", false, "05/05/2021", false, "NSW", 4 },
        new object[] { "28/04/2021", false, "05/05/2021", false, "QLD", 3 },
        new object[] { "16/07/2021", false, "19/07/2021", false, "NSW", 0},
        new object[] { "16/07/2021", true, "19/07/2021", true, "NSW", 2},
      };
  }
}
