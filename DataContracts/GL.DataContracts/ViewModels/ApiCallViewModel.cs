﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GL.DataContracts.ViewModels
{
    public class ApiCallViewModel
    {
        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("Jurisdiction")]
        public string State { get; set; }

        [JsonProperty("Applicable To")]
        public string ApplicableTo { set { State = value; } }
        //public string ApplicableTo { set; get; }
    }
}
