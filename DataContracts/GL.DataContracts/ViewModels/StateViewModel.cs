using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;

namespace GL.DataContracts.ViewModels
{
  public class StateViewModel
  {
    public string Code { get; set; }
    public string Name { get; set; }
    public string CapitalCity {get; set; }
  }
}
