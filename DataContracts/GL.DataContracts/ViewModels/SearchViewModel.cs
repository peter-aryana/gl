﻿using System;

namespace GL.DataContracts.ViewModels
{
    public class SearchViewModel
    {
        public string DateFrom { get; set; }
        public DateTime DateFromParsed { get { return DateTime.ParseExact(DateFrom, "yyyyMMdd", null); }}
        public bool IncludeDateFrom { get; set; }
        public string DateTo { get; set; }
        public DateTime DateToParsed { get { return DateTime.ParseExact(DateTo, "yyyyMMdd", null); } }
        public bool IncludeDateTo { get; set; }
        public string State {get;set;}
    }
}
