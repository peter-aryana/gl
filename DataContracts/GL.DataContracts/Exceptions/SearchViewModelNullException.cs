﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GL.DataContracts.Exceptions
{
    public class SearchViewModelNullException : ApplicationException
    { 
        public SearchViewModelNullException(string message) : base(message)
        {

        }

        public SearchViewModelNullException(string message, Exception ex) : base(message, ex)
        {

        }
    }
}
